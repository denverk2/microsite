(function(window, undefined, $) {

    var queryStringUtility = window.queryStringUtility = {

        getQueryStringValueByKeyName: function(keyName) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + keyName + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        /*
            url parameter: If no URL is supplied, it will use window.location

            remove param:   Not supplying a value will remove the parameter
            Add / Update:   supplying one will add/update the parameter
        */
        setQueryStringKeyValuePair: function(key, value, url) {
            if (!url) url = window.location.href;
            var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
                hash;

            if (re.test(url)) {
                if (typeof value !== 'undefined' && value !== null)
                    return url.replace(re, '$1' + key + "=" + value + '$2$3');
                else {
                    hash = url.split('#');
                    url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
                        url += '#' + hash[1];
                    return url;
                }
            } else {
                if (typeof value !== 'undefined' && value !== null) {
                    var separator = url.indexOf('?') !== -1 ? '&' : '?';
                    hash = url.split('#');
                    url = hash[0] + separator + key + '=' + value;
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                        url += '#' + hash[1];
                    return url;
                } else
                    return url;
            }
        },

        /*
            This will take a querystring with the '?' stripped and return 
            an array of objects
        */
        getQueryStringKeyValuePairs: function(querystringStripped) {
            var querystringArray = querystringStripped.split('&'),
                keyValueArray = [],
                i = 0;

            $.each(querystringArray, function(index, value) {
                var keyValueString = value,
                    keyValArray = value.split('=');

                if (keyValArray.length == 2) {
                    var key = keyValArray[0],
                        val = keyValArray[1];

                    //TODO: Move the pair object into the global
                    //namespace inside of the SEJ (self executing JS)
                    var pair = {};
                    pair[key] = val;

                    keyValueArray[i] = pair;
                    i++;
                }
            });

            return keyValueArray;
        },

        addExistingQuerystringToLinksOnPage: function() {
            //declare local vars
            var querystringStripped = "",
                hashValue = '',
                querystringKeyVals,
                querystringStrippedParts,
                existingQuerystringParamsFromUrl = '',

                //get entire querystring including '?'
                querystring = window.location.search;

            //if there is a querystring
            if (querystring.length) {
                //strip the '?'
                querystringStripped = querystring.substr(1, querystring.length);

                $('a').each(function() {
                    var href = $(this).attr('href');

                    //the link has an existing querystring in it but no hash
                    if ((href !== undefined) && (href.indexOf('?') !== -1) && (href.indexOf('#') === -1)) {

                        //append incoming URL qs onto link href
                        href += (href.match(/\?/) ? '&' : '?') + querystringStripped;

                        //set the new href on the link
                        $(this).attr('href', href);

                    }
                    //the link has an existing querystring in it and a hash
                    else if ((href !== undefined) && (href.indexOf('?') !== -1) && (href.indexOf('#') !== -1)) {

                        var hrefQuerystringPartsWithHash = href.split('?'),
                            hrefQsParts = hrefQuerystringPartsWithHash[1].split('#');

                        href = hrefQuerystringPartsWithHash[0]; + '?' + hrefQuerystringPartsWithHash[1].split('#').pop();

                        //append URL querystring to href
                        href += (href.match(/\?/) ? '&' : '?') + querystringStripped;

                        $(this).attr('href', href);

                    }
                    //the link has NO querystring in it but has a hash
                    else if ((href !== undefined) && (href.indexOf('?') === -1) && (href.indexOf('#') !== -1)) {

                        var hrefPartsWithHash = href.split('#');

                        href = hrefPartsWithHash[0]; + '?' + hrefPartsWithHash[1].split('#').pop();

                        href += '?' + querystringStripped;

                        $(this).attr('href', href);

                    } else if (href !== undefined) //there is no querystrinbg or hash
                    {
                        href += '?' + querystringStripped;
                        $(this).attr('href', href);
                    }
                });

            }
        }

    }; //end querystring namespace

    var helperFunctions = window.helperFunctions = {

        getDeduplicatedList: function(arrayToDeDupe) {
            var temp = [],
                result = [];

            $.each(arrayToDeDupe, function(index, element) {
                if ($.inArray(element, temp) == -1) {
                    temp.push(element);
                }
            });

            $.each(temp, function(index, element) {
                if (element !== undefined) {
                    result.push(element);
                }
            });

            return result;
        }

    }; //end helperFunctions Namespace

    queryStringUtility.addExistingQuerystringToLinksOnPage();

})(window, undefined, jQuery);