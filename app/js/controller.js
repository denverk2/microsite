'use strict';

// document.domain='k2.com';

console.log('controller.js loaded successfully');

function scrollContent() {
    console.log('scrollContent got registered');

    if ($(window).width() >= 800) {
       console.log('browser is greater than 800px'); 
       $("html,body").animate({ scrollTop: 455 }, "slow"); 
    } else if ($(window).width() >= 600) {
        console.log('browser is greater than 600px');
        $("html,body").animate({ scrollTop: 305 }, "slow"); 
    } else if ($(window).width() < 600 ) {
        console.log('browser is less than 600px');
        $("html,body").animate({ scrollTop: 270 }, "slow"); 
    }
    // $('html, body').animate({scrollTop:$('#scrollDiv').position().top}, 'slow');
} 

function removeClass() {
    $('.bne').removeClass('open');  
}

function firstDownloadOverride() {
    $('#firstDownload').css('display', 'block');
    console.log('firstDownloadOverride was triggered');

    if ($(window).width() >= 800) {
       console.log('browser is greater than 800px'); 
       $("html,body").animate({ scrollTop: 455 }, "slow"); 
    } else if ($(window).width() >= 600) {
        console.log('browser is greater than 600px');
        $("html,body").animate({ scrollTop: 305 }, "slow"); 
    } else if ($(window).width() < 600 ) {
        console.log('browser is less than 600px');
        $("html,body").animate({ scrollTop: 270 }, "slow"); 
    }

}

function firstDownloadOverrideTwo() {
    $('#firstDownload').css('display', 'none');
    console.log('firstDownloadOverrideTwo was triggered');
}

/* Angular PDF Configuration */

var app = angular.module('microsite', []);
    
app.controller('pdfControllerSembcorpCaseStudy', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/SCNA_SembcorpIndustries_CS_A4_Digital_112315.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'SCNA_SembcorpIndustries_CS_A4_Digital_112315.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('rocheDiagnosticsCaseStudy', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('roche Diagnostics controller was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_CaseStudy_Roche_DIGITAL.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_CaseStudy_Roche_DIGITAL.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('topFivePurchasingProcesses', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('top five purchase prosses controller was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/Purchasing_Infographic_022316_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'Purchasing_Infographic_022316_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('topFiveFinanceProcesses', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('top five purchase prosses controller was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/Finance_Infographic_021616_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'Finance_Infographic_021616_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('southWestBankCaseStudy', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('roche Diagnostics controller was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/SouthwestBank_CS_A4_Digital.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'SouthwestBank_CS_A4_Digital.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerKinrossGoldCaseStudy', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('roche Diagnostics controller was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_Kinross_CaseStudy_A4_DIGITAL.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_Kinross_CaseStudy_A4_DIGITAL.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerMetroPacificCaseStudy', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('roche Diagnostics controller was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/Metro-Pacific-CS-letter-screen.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'Metro-Pacific-CS-letter-screen.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('webrootCaseStudy', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('roche Diagnostics controller was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_CaseStudy_Webroot_A4_DIGITAL.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_CaseStudy_Webroot_A4_DIGITAL.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerItInfographic', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/IT_Infographic_022416_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'IT_Infographic_022416_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerPurchasingInfographic', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/IT_Infographic_022416_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'IT_Infographic_022416_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerFinanceInfographic', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/Finance_Infographic_021616_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'Finance_Infographic_021616_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerHrInfographic', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/HR_Infographic_021616_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'HR_Infographic_021616_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerOperationsInfographic', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/Operations_Infographic_021616_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'Operations_Infographic_021616_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('pdfControllerSalesMarketingInfographic', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/SalesMarketing_Infographic_021616_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'SalesMarketing_Infographic_021616_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

// eguides
app.controller('itEguide', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_eGuide_IT_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_eGuide_IT_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('hrEguide', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/HR_Infographic_021616_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'HR_Infographic_021616_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);


app.controller('operationsEguide', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_eGuide_Operations_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_eGuide_Operations_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('financeEguide', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_eGuide_Finance_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_eGuide_Finance_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('purchasingEguide', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_eGuide_Purchasing_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_eGuide_Purchasing_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);

app.controller('salesMarketingEguide', ['$scope', function($scope) {
    $scope.downloadFile = function($event) { 
        console.log('pdfController was received on ng-click');

        var link = document.createElement("a");
        var url = '../pdf-files/K2_eGuide_SalesAndMarketing_Final.pdf';

        link.setAttribute("href", url);
        link.setAttribute("download", 'K2_eGuide_SalesAndMarketing_Final.pdf');

        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
}]);
/*angular
    .module("microsite", ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
                .state('it', {
                    url: '/',
                    templateUrl: 'it.html',
                    onEnter: removeClass
                })    
                    .state('it.introvideo', {
                        url: 'it/intro-video',
                        templateUrl: 'templates/introvideo.html',
                        onEnter: scrollContent,
                        onExit: firstDownloadOverrideTwo
                    }) 
                    .state('it.designing-workflows', {
                        url: 'it/designing-workflows',
                        templateUrl: 'templates/designingWorkflows.html',
                        // onEnter: scrollContent,
                        onEnter: firstDownloadOverride,
                        onExit: firstDownloadOverrideTwo
                    }) 

                    .state('it.runningprocesses', {
                        url: 'it/runningprocesses',
                        templateUrl: 'templates/runningProcesses.html',
                        onEnter: firstDownloadOverride,
                        onExit: firstDownloadOverrideTwo,
                        controller: function ($scope, $state, $http) {
                            $scope.user = {};

                            $scope.actions = {
                                submitForm: function () {
                                    $http({
                                      method: 'POST',
                                      data: $scope.user,
                                      // url: 'http://content.k2.com/Director.aspx?sid=3469&amp;sky=Q1MI3J4J&amp;pgi=6867&amp;pgk=RIE68PC2&amp;eli=FBACEBB55BE771B93616F421CDB5730BECA4D79431135361&amp;rid=387524&amp;rky=9MZ5LG3T&amp;tky=131001345670708416&amp;fbs=1F518819F034F3AD566FB199C5DFE088720DAD33633FAEB3'
                                      url: 'http://www.k2.com/forms/submit.xml'
                                    }).
                                    then(function () {
                                        // success
                                      //$scope.view.showSuccessMessage = true;
                                      $state.go('successMessage');
                                    });
                                }
                            };
                        }
                    }) 
                    .state('it.submitrunningprocesses', {
                        url: 'it/submitrunningprocesses',
                        templateUrl: 'templates/submitRunningProcesses.html',
                        onEnter: scrollContent,
                        onExit: firstDownloadOverrideTwo
                    })    

                    .state('it.sembcorp', {
                        url: 'it/sembcorp',
                        templateUrl: 'templates/sembcorp-case-study.html',
                        onEnter: firstDownloadOverride,
                        onExit: firstDownloadOverrideTwo,
                        controller: function($scope, $state) {
                            $scope.downloadFile = function($event) { 
                                var link = document.createElement("a");
                                var url = '/pdf-files/K2_CaseStudy_Roche_DIGITAL.pdf';

                                link.setAttribute("href", url);
                                link.setAttribute("download", 'SCNA_SembcorpIndustries_CS_A4_Digital_112315.pdf');

                                var event = document.createEvent('MouseEvents');
                                event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                                link.dispatchEvent(event);

                                $state.go('it.sembcorp-confirm');
                            };
                        }
                    }) 
                    .state('it.sembcorp-confirm', {
                        url: 'it/sembcorp-confirmation',
                        templateUrl: 'templates/successfulDownloadsembcorp-case-study.html',
                        onEnter: scrollContent,
                        onExit: firstDownloadOverrideTwo
                    })    

                    .state('it.enterprisesoftware', {
                        url: 'it/enterprisesoftware',
                        templateUrl: 'templates/enterprisesoftware.html',
                        onEnter: firstDownloadOverride,
                        onExit: firstDownloadOverrideTwo  
                    })    

                .state('purchasing', {
                    url: '/purchasing',
                    templateUrl: 'purchasing.html',
                    onEnter: removeClass
                })    

                    .state('purchasing.introvideo', {
                        url: '/intro-video',
                        templateUrl: 'templates/introvideo.html',
                        onEnter: scrollContent
                    })
                    .state('purchasing.designing-workflows', {
                        url: '/designing-workflows',
                        templateUrl: 'templates/designingWorkflows.html',
                        onEnter: scrollContent
                    }) 
                    .state('purchasing.designing-forms', {
                        url: '/designing-forms',
                        templateUrl: 'templates/designingForms.html',
                        onEnter: scrollContent
                    }) 
                    .state('purchasing.runningprocesses', {
                        url: '/runningprocesses',
                        templateUrl: 'templates/runningProcesses.html',
                        onEnter: scrollContent,
                        controller: function ($scope, $state, $http) {
                            $scope.user = {};

                            $scope.actions = {
                                submitForm: function () {
                                    $http({
                                      method: 'POST',
                                      data: $scope.user,
                                      // url: 'http://content.k2.com/Director.aspx?sid=3469&amp;sky=Q1MI3J4J&amp;pgi=6867&amp;pgk=RIE68PC2&amp;eli=FBACEBB55BE771B93616F421CDB5730BECA4D79431135361&amp;rid=387524&amp;rky=9MZ5LG3T&amp;tky=131001345670708416&amp;fbs=1F518819F034F3AD566FB199C5DFE088720DAD33633FAEB3'
                                      url: 'http://www.k2.com/forms/submit.xml'
                                    }).
                                    then(function () {
                                        // success
                                      //$scope.view.showSuccessMessage = true;
                                      $state.go('successMessage');
                                    });
                                }
                            };
                        }
                    }) 
                    .state('purchasing.submitrunningprocesses', {
                        url: '/submitrunningprocesses',
                        templateUrl: 'templates/submitRunningProcesses.html',
                        onEnter: scrollContent
                    })      
                    .state('purchasing.roche', {
                        url: '/roche',
                        templateUrl: 'templates/roche.html',
                        onEnter: scrollContent,
                        controller: function($scope, $state) {
                            $scope.downloadFile = function($event) { 
                                var link = document.createElement("a");
                                var url = '/pdf-files/K2_CaseStudy_Roche_DIGITAL.pdf';

                                link.setAttribute("href", url);
                                link.setAttribute("download", 'K2_CaseStudy_Roche_DIGITAL.pdf');

                                var event = document.createEvent('MouseEvents');
                                event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                                link.dispatchEvent(event);

                                $state.go('purchasing.successfulDownloadRoche');
                            };
                        }
                    }) 
                    .state('purchasing.successfulDownloadRoche', {
                        url: '/roche-confirmation',
                        templateUrl: 'templates/successfulDownloadRoche.html',
                        onEnter: scrollContent
                    })    

                .state('hr', {
                    url: '/hr',
                    templateUrl: 'hr.html',
                    onEnter: removeClass
                })

                    .state('hr.introvideo', {
                        url: '/intro-video',
                        templateUrl: 'templates/introvideo.html',
                        onEnter: scrollContent
                    })  
                    .state('hr.designing-workflows', {
                        url: '/designing-workflows',
                        templateUrl: 'templates/designingWorkflows.html',
                        onEnter: scrollContent
                    }) 
                    .state('hr.designing-forms', {
                        url: '/designing-forms',
                        templateUrl: 'templates/designingForms.html',
                        onEnter: scrollContent
                    })    
                    .state('hr.runningprocesses', {
                        url: '/runningprocesses',
                        templateUrl: 'templates/runningProcesses.html',
                        onEnter: scrollContent,
                        controller: function ($scope, $state, $http) {
                            $scope.user = {};

                            $scope.actions = {
                                submitForm: function () {
                                    $http({
                                      method: 'POST',
                                      data: $scope.user,
                                      // url: 'http://content.k2.com/Director.aspx?sid=3469&amp;sky=Q1MI3J4J&amp;pgi=6867&amp;pgk=RIE68PC2&amp;eli=FBACEBB55BE771B93616F421CDB5730BECA4D79431135361&amp;rid=387524&amp;rky=9MZ5LG3T&amp;tky=131001345670708416&amp;fbs=1F518819F034F3AD566FB199C5DFE088720DAD33633FAEB3'
                                      url: 'http://www.k2.com/forms/submit.xml'
                                    }).
                                    then(function () {
                                        // success
                                      //$scope.view.showSuccessMessage = true;
                                      $state.go('successMessage');
                                    });
                                }
                            };
                        }
                    }) 
                    .state('hr.submitrunningprocesses', {
                        url: '/submitrunningprocesses',
                        templateUrl: 'templates/submitRunningProcesses.html',
                        onEnter: scrollContent
                    }) 
                    .state('hr.webroot', {
                        url: '/webroot',
                        templateUrl: 'templates/webroot.html',
                        onEnter: scrollContent,
                        controller: function($scope, $state) {
                            $scope.downloadFile = function($event) { 
                                var link = document.createElement("a");
                                var url = '/pdf-files/K2_CaseStudy_Webroot_A4_DIGITAL.pdf';

                                link.setAttribute("href", url);
                                link.setAttribute("download", 'K2_CaseStudy_Webroot_A4_DIGITAL.pdf');

                                // Simulate clicking the download link
                                var event = document.createEvent('MouseEvents');
                                event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                                link.dispatchEvent(event);

                                $state.go('hr.successfulDownloadWebroot');
                            };
                        }
                    })                    
                    .state('hr.successfulDownloadWebroot', {
                        url: '/webroot-confirmation',
                        templateUrl: 'templates/successfulDownloadWebroot.html',
                        onEnter: scrollContent
                    })

                .state('finance', {
                    url: '/finance',
                    templateUrl: 'finance.html',
                    onEnter: removeClass
                })

                    .state('finance.introvideo', {
                        url: '/intro-video',
                        templateUrl: 'templates/introvideo.html',
                        onEnter: scrollContent
                    })     
                    .state('finance.designing-forms', {
                        url: '/designing-forms',
                        templateUrl: 'templates/designingForms.html',
                        onEnter: scrollContent
                    })     
                    .state('finance.runningprocesses', {
                        url: '/runningprocesses',
                        templateUrl: 'templates/runningProcesses.html',
                        onEnter: scrollContent,
                        controller: function ($scope, $state, $http) {
                            $scope.user = {};

                            $scope.actions = {
                                submitForm: function () {
                                    $http({
                                      method: 'POST',
                                      data: $scope.user,
                                      // url: 'http://content.k2.com/Director.aspx?sid=3469&amp;sky=Q1MI3J4J&amp;pgi=6867&amp;pgk=RIE68PC2&amp;eli=FBACEBB55BE771B93616F421CDB5730BECA4D79431135361&amp;rid=387524&amp;rky=9MZ5LG3T&amp;tky=131001345670708416&amp;fbs=1F518819F034F3AD566FB199C5DFE088720DAD33633FAEB3'
                                      url: 'http://www.k2.com/forms/submit.xml'
                                    }).
                                    then(function () {
                                        // success
                                      //$scope.view.showSuccessMessage = true;
                                      $state.go('successMessage');
                                    });
                                }
                            };
                        }
                    })
                .state('finance.submitrunningprocesses', {
                    url: '/running-processes-confirmation',
                    templateUrl: 'templates/submitRunningProcesses.html',
                    onEnter: scrollContent
                })  
                .state('finance.kinross', {
                    url: '/kinross-gold-case-study',
                    templateUrl: 'templates/kinross.html',
                    onEnter: scrollContent,
                    controller: function($scope, $state) {
                        $scope.downloadFile = function($event) { 
                            var link = document.createElement("a");
                            var url = '/pdf-files/K2_Kinross_CaseStudy_A4_DIGITAL.pdf';

                            link.setAttribute("href", url);
                            link.setAttribute("download", 'K2_Kinross_CaseStudy_A4_DIGITAL.pdf');

                            // Simulate clicking the download link
                            var event = document.createEvent('MouseEvents');
                            event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                            link.dispatchEvent(event);

                            $state.go('finance.successfulDownloadKinross');
                        };
                    }
                })   
                .state('finance.successfulDownloadKinross', {
                    url: '/kinross-gold-case-study-confirmation',
                    templateUrl: 'templates/successfulDownloadKinross.html',
                    onEnter: scrollContent
                })      

            .state('operations', {
                url: '/operations',
                templateUrl: 'operations.html',
                onEnter: removeClass
            })
                .state('operations.introvideo', {
                    url: '/intro-video',
                    templateUrl: 'templates/introvideo.html',
                    onEnter: scrollContent
                })    
                .state('operations.designing-workflows', {
                    url: '/designing-workflows',
                    templateUrl: 'templates/designingWorkflows.html',
                    onEnter: scrollContent
                }) 
                .state('operations.designing-forms', {
                    url: '/designing-forms',
                    templateUrl: 'templates/designingForms.html',
                    onEnter: scrollContent
                })    
                .state('operations.runningprocesses', {
                    url: '/runningprocesses',
                    templateUrl: 'templates/runningProcesses.html',
                    onEnter: scrollContent,
                    controller: function ($scope, $state, $http) {
                        $scope.user = {};

                        $scope.actions = {
                            submitForm: function () {
                                $http({
                                  method: 'POST',
                                  data: $scope.user,
                                  // url: 'http://content.k2.com/Director.aspx?sid=3469&amp;sky=Q1MI3J4J&amp;pgi=6867&amp;pgk=RIE68PC2&amp;eli=FBACEBB55BE771B93616F421CDB5730BECA4D79431135361&amp;rid=387524&amp;rky=9MZ5LG3T&amp;tky=131001345670708416&amp;fbs=1F518819F034F3AD566FB199C5DFE088720DAD33633FAEB3'
                                  url: 'http://www.k2.com/forms/submit.xml'
                                }).
                                then(function () {
                                    // success
                                  //$scope.view.showSuccessMessage = true;
                                  $state.go('successMessage');
                                });
                            }
                        };
                    }
                }) 
                .state('operations.submitrunningprocesses', {
                    url: '/submitrunningprocesses',
                    templateUrl: 'templates/submitRunningProcesses.html',
                    onEnter: scrollContent
                })  
                .state('operations.metropacific', {
                    url: '/metro-pacific-case-study',
                    templateUrl: 'templates/metropacific.html',
                    onEnter: scrollContent,
                    controller: function($scope, $state) {
                        $scope.downloadFile = function($event) { 
                            var link = document.createElement("a");
                            var url = '/pdf-files/Metro-Pacific-CS-letter-screen.pdf';

                            link.setAttribute("href", url);
                            link.setAttribute("download", 'Metro-Pacific-CS-letter-screen.pdf');

                            // Simulate clicking the download link
                            var event = document.createEvent('MouseEvents');
                            event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                            link.dispatchEvent(event);

                            $state.go('operations.successfulDownloadMetropacific');
                        };
                    }
                }) 
                .state('operations.successfulDownloadMetropacific', {
                    url: '/metro-pacific-case-study-confirmation',
                    templateUrl: 'templates/successfulDownloadMetropacific.html',
                    onEnter: scrollContent
                }) 

            .state('sales-marketing', {
                url: '/sales-marketing',
                templateUrl: 'sales-marketing.html',
                onEnter: removeClass
            })
                .state('sales-marketing.introvideo', {
                    url: '/intro-video',
                    templateUrl: 'templates/introvideo.html',
                    onEnter: scrollContent
                }) 
                .state('sales-marketing.designing-workflows', {
                    url: '/designing-workflows',
                    templateUrl: 'templates/designingWorkflows.html',
                    onEnter: scrollContent
                }) 
                .state('sales-marketing.designing-forms', {
                    url: '/designing-forms',
                    templateUrl: 'templates/designingForms.html',
                    onEnter: scrollContent
                })     

                .state('sales-marketing.runningprocesses', {
                    url: '/runningprocesses',
                    templateUrl: 'templates/runningProcesses.html',
                    onEnter: scrollContent,
                    controller: function ($scope, $state, $http) {
                        $scope.user = {};

                        $scope.actions = {
                            submitForm: function () {
                                $http({
                                  method: 'POST',
                                  data: $scope.user,
                                  // url: 'http://content.k2.com/Director.aspx?sid=3469&amp;sky=Q1MI3J4J&amp;pgi=6867&amp;pgk=RIE68PC2&amp;eli=FBACEBB55BE771B93616F421CDB5730BECA4D79431135361&amp;rid=387524&amp;rky=9MZ5LG3T&amp;tky=131001345670708416&amp;fbs=1F518819F034F3AD566FB199C5DFE088720DAD33633FAEB3'
                                  url: 'http://www.k2.com/forms/submit.xml'
                                }).
                                then(function () {
                                    // success
                                  //$scope.view.showSuccessMessage = true;
                                  $state.go('successMessage');
                                });
                            }
                        };
                    }
                }) 
                .state('sales-marketing.submitrunningprocesses', {
                    url: '/submitrunningprocesses',
                    templateUrl: 'templates/submitRunningProcesses.html',
                    onEnter: scrollContent
                })
                .state('sales-marketing.southwest', {
                    url: '/southwest-case-study',
                    templateUrl: 'templates/southwest.html',
                    onEnter: scrollContent,
                    controller: function($scope, $state) {
                        $scope.downloadFile = function($event) { 
                            var link = document.createElement("a");
                            var url = '/pdf-files/SouthwestBank_CS_A4_Digital.pdf';

                            link.setAttribute("href", url);
                            link.setAttribute("download", 'SouthwestBank_CS_A4_Digital.pdf');

                            // Simulate clicking the download link
                            var event = document.createEvent('MouseEvents');
                            event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                            link.dispatchEvent(event);

                            $state.go('sales-marketing.successfulDownloadSouthwest');
                        };
                    }
                })
                .state('sales-marketing.successfulDownloadSouthwest', {
                    url: '/southwest-case-study-confirmation',
                    templateUrl: 'templates/successfulDownloadSouthwest.html',
                    onEnter: scrollContent 
                }) 

            .state('contact-us', {
                url: '/contact-us',
                templateUrl: 'templates/contact-us.html',
                onEnter: scrollContent 
            });     


            $urlRouterProvider.otherwise('/');
            // $locationProvider.html5Mode(true).hashPrefix('');
            // $locationProvider.html5Mode(true);
    }]);  */

