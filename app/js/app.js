// document.domain='k2.com';

$(document).ready(function() {
    $('#menu-icon').on('click', function() {
        console.log('click went through');       

        if($('.bne').hasClass('open')) {
            $('.bne').removeClass('open');
        } 
        else {
            $('.bne').addClass('open');
        }       
    });

    // $('#menu-icon').on('click', function() {
    //     console.log('click went through');        
    //     $('.bne').removeClass('open');    
    // });

    $('.download__button').on('click', function() {
        console.log('download button click went through'); 
        $('#firstDownload').removeClass('hideOnLoad');
    });

    $('.videoBtnTwo').css('display', 'block');


    // remove related asset if on that asset page
    function removeDuplicateAsset() {

        // split url down to name of page
        var currentPage = document.location.href,
            splitUrl = currentPage.toString().split('/'),
            pagename = splitUrl.pop().split('.'),
            relatedAssets = $('.download__downloadVerticalCentering figure'),
            currentAssetLink;

        // edge case if the URL has -confirmation in the name
        if(pagename[0].match(/-confirmation/g)) {
            pagename = pagename[0].split('-confirmation');
        }

        // Loop through related assets and spliut href from anchor
        $(relatedAssets).each(function() {
            // split asset link for name of page
            currentInlineAssetLink = $(this).find('a').attr('href');                

            var splitInlineAssetLink = currentInlineAssetLink.split('/');
                getInlineAssetPageName = splitInlineAssetLink.pop().split('.');

            // Check if asset URL matches page URL
            if(getInlineAssetPageName[0] == pagename[0]) {
                $(this).remove();
            }
        }); 
    };

    removeDuplicateAsset();














    switch (window.location.pathname) {
        case '/':
        case '/index.html':
        case '/it.html':
        case '/it/intro-video.html':
        case '/it/it-infographic.html':
        case '/it/it-infographic-confirmation.html':
        case '/it/running-processes.html':
        case '/it/running-processes-confirmation.html':
        case '/it/sembcorp-case-study.html':
        case '/it/sembcorp-case-study-confirmation.html':
        case '/it/enterprise-software.html':
        case '/it/enterprise-software-confirmation.html':
        case '/it/designing-workflows.html':
        case '/it/designing-workflows-confirmation.html':                         
            $('.it-menu-item').addClass('active-green');
            break;

        case '/purchasing.html':
        case '/purchasing/intro-video.html':
        case '/purchasing/purchasing-infographic.html':
        case '/purchasing/purchasing-infographic-confirmation.html':
        case '/purchasing/running-processes.html':
        case '/purchasing/running-processes-confirmation.html':
        case '/purchasing/roche-diagnostics-case-study.html':
        case '/purchasing/roche-diagnostics-case-study-confirmation.html':
        case '/purchasing/designing-forms-confirmation.html':
        case '/purchasing/designing-forms.html':
        case '/purchasing/designing-workflows-confirmation.html':
        case '/purchasing/designing-workflows.html':
            $('.purchasing-menu-item').addClass('active-blue');
            break;

        case '/hr.html':
        case '/hr/intro-video.html':
        case '/hr/hr-infographic.html':
        case '/hr/hr-infographic-confirmation.html':
        case '/hr/running-processes.html':
        case '/hr/running-processes-confirmation.html':
        case '/hr/designing-workflows.html':
        case '/hr/designing-workflows-confirmation.html': 
        case '/hr/designing-forms.html':
        case '/hr/designing-forms-confirmation.html':
        case '/hr/webroot-case-study.html':
        case '/hr/webroot-case-study-confirmation.html': 
            $('.hr-menu-item').addClass('active-green');
            break;

        case '/finance.html':
        case '/finance/intro-video.html':
        case '/finance/finance-infographic.html':
        case '/finance/finance-infographic-confirmation.html':
        case '/finance/running-processes.html':
        case '/finance/running-processes-confirmation.html':
        case '/finance/kinross-gold-case-study.html':
        case '/finance/kinross-gold-case-study-confirmation.html':
        case '/finance/expense-claim.html':
        case '/finance/expense-claim-confirmation.html':
        case '/finance/designing-forms-confirmation.html':
        case '/finance/designing-forms.html':        
            $('.finance-menu-item').addClass('active-blue');
            break;

        case '/operations.html':
        case '/operations/intro-video.html':
        case '/operations/operations-infographic.html':
        case '/operations/operations-infographic-confirmation.html':
        case '/operations/running-processes.html':
        case '/operations/running-processes-confirmation.html':
        case '/operations/metro-pacific-case-study.html':
        case '/operations/metro-pacific-case-study-confirmation.html':
        case '/operations/designing-workflows.html':
        case '/operations/designing-workflows-confirmation.html': 
        case '/operations/designing-forms.html':
        case '/operations/designing-forms-confirmation.html':        
            $('.operations-menu-item').addClass('active-green');
            break;

        case '/sales-marketing.html':
        case '/sales-marketing/intro-video.html':
        case '/sales-marketing/sales-marketing-infographic.html':
        case '/sales-marketing/sales-marketing-infographic-confirmation.html':        
        case '/sales-marketing/running-processes.html':
        case '/sales-marketing/running-processes-confirmation.html': 
        case '/sales-marketing/designing-workflows.html':
        case '/sales-marketing/designing-workflows-confirmation.html': 
        case '/sales-marketing/designing-forms.html':
        case '/sales-marketing/designing-forms-confirmation.html':       
        case '/sales-marketing/southwest-bank.html':       
        case '/sales-marketing/southwest-bank-confirmation.html':       
            $('.salesmarketing-menu-item').addClass('active-blue');
            break;    
    }

    switch(window.location.pathname) {
        case '/':
            $('#firstDownload').addClass('hideOnLoad');
            console.log('this went through')
            break;
        case '/it.html':
            $('#firstDownload').addClass('hideOnLoad');
            break;
        case '/purchasing.html':
            $('#firstDownload').addClass('hideOnLoad');
            break;
        case '/hr.html':
            $('#firstDownload').addClass('hideOnLoad');
            break;
        case '/finance.html':
            $('#firstDownload').addClass('hideOnLoad');
            break;
        case '/operations.html':
            $('#firstDownload').addClass('hideOnLoad');
            break;
        case '/sales-marketing.html':
            $('#firstDownload').addClass('hideOnLoad');
            break;    
        default:
            $('#firstDownload').addClass('activeIframe');
            console.log('default case');
            break;
    }
    
});