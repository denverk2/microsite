'use strict';
var _ = require('lodash');
var path = require('path');
var fs = require('fs-extra');


module.exports = function(grunt) {


    function assembleHtmlPages( datasrc,template,name ) {


        var data = grunt.file.readJSON( datasrc );
        var pageArray = [];

        for ( var i = 0; i < data[name].length; i++) {
          pageArray.push({
            // the filename will determine how the page is named later
            filename: "" + data[name].name, // name file based off name property
            // the data for the current step from the json file
            data: data[name][i],
            // add the template as the page content
            content: grunt.file.read( template )
          });
        }
        return _.flatten(pageArray)
    }

    // Project configuration.
    // import object into initConfig
    grunt.initConfig({

        //===================//
        // read dependencies //
        //===================//
        pkg : grunt.file.readJSON('package.json'),

        //========//
        // config //
        //========//
        project: {
          app       : 'app',
          dev       : 'app',
          deploy    : 'deploy',
          css       : '<%= project.dev %>/css',
          scss      : '<%= project.dev %>/scss',
          js        : '<%= project.dev %>/js'
        },

       

    //======================================//
    // Copy files from one cycle to another //
    //======================================//
    copy: {
        //== QC stage ==//
        deploy: {
            expand  : true,
            cwd     : '<%= project.dev %>/',
            src     : [ 'css/**',
                        'js/**/*',
                        'js/vendor/*',
                        'img/**/*',
                        'fonts/**/*',
                        'scss/**/*',
                        '*.html'
                      ],
            flatten : false,
            filter  : 'isFile',
            dest    : '<%= project.stage %>/'
        }
    },

    //=====================================//
    // Sync directories for Project Mayhem //
    //=====================================//
    sync: {
        main: {
            files: [{
                cwd: 'app',
                src: [ '**', '../Gruntfile.js', '../package.json' ], // to exclude files from sync use "!" before file path
                dest: '<%= proj.syncDest %>\\dev'
            }],
            verbose: true,
            failOnError: true
        }
    },

    //=========================================//
    // Set up local web server, and livereload //
    //=========================================//
    connect: {
        dev: {
            options: {
              port:9002,
              hostname: 'localhost',
              base:'./app/',
              open:true,
              livereload:35729
            }
        },
        deploy: {
            options: {
              port: 9003,
              hostname: 'localhost',
              base:'<%= project.deploy %>'
            }
        }
    },

    //===============//
    // SCSS compiler //
    //===============//
    //== The watch only needs to be on Dev as this folder ==//
    //== Is the only one where active Dev occurs.         ==//
    sass: {
        dev: {
            options: {
              style: 'expanded'
            },
            files: {
              '<%= project.css %>/app.css' : '<%= project.scss %>/app.scss'
            }
        }
    },

    //==========================================//
    // File watch to trigger livereload on save //
    //==========================================//
    watch: {
        // sass: {
        //     files: '<%= project.dev %>/scss/**/*.scss',
        //     tasks: ['sass:dev']
        // },
        livereload: {
            files: [
                '<%= project.dev %>/**/*.html',
                '<%= project.css %>/*.css',
                '<%= project.js %>/**/*.js'
            ],
            options: {
              livereload: true
            },
        }
    },


    // assemble: {
    //     options: {
    //         flatten: true, // falttens Dir structure under main dist folder.
    //         prettify: {
    //           indent: 4,
    //           condense: true,
    //           newlines: true
    //         },
    //         // assets: 'dev/dist', // Used with the {{assets}} variable to resolve the relative path from the dest file to the assets folder.
    //         // helpers: 'templates/helpers/*.js',
    //         partials: ['dev/hbs/**/*.hbs'], // bring in all partials
    //         layoutdir: 'dev/hbs/layout/', // used as the CWD for layout
    //     },
    //     headerModule: {
    //         options: {
    //             pages: assembleHtmlPages( 'dev/hbs/data/headerModule/headerModule-data.json','dev/hbs/components/headerModule/headerModule.hbs', 'headerModule' )
    //         },
    //         files: [
    //             { dest: 'dev/dist/headerModule/headerModule.html', src : '!*' }
    //         ]
    //     },
    //     titleModule: {
    //         options: {
    //             pages: assembleHtmlPages( 'dev/hbs/data/titleModule/titleModule-data.json','dev/hbs/components/titleModule/titleModule.hbs', 'titleModule' )
    //         },
    //         files: [
    //             { dest: 'dev/dist/titleModule/titleModule.html', src : '!*' }
    //         ]
    //     },
    //     assetModule: {
    //         options: {
    //             pages: assembleHtmlPages( 'dev/hbs/data/assetModule/assetModule-data.json','dev/hbs/components/assetModule/assetModule.hbs', 'assetModule' )
    //         },
    //         files: [
    //             { dest: 'dev/dist/assetModule/assetModule.html', src : '!*' }
    //         ]
    //     },
    //     socialModule: {
    //         options: {
    //             pages: assembleHtmlPages( 'dev/hbs/data/socialModule/socialModule-data.json','dev/hbs/components/socialModule/socialModule.hbs', 'socialModule' )
    //         },
    //         files: [
    //             { dest: 'dev/dist/socialModule/socialModule.html', src : '!*' }
    //         ]
    //     },
    //     footerModule: {
    //         options: {
    //             pages: assembleHtmlPages( 'dev/hbs/data/footerModule/footerModule-data.json','dev/hbs/components/footerModule/footerModule.hbs', 'footerModule' )
    //         },
    //         files: [
    //             { dest: 'dev/dist/footerModule/footerModule.html', src : '!*' }
    //         ]
    //     }
    // },
    concat: {
        dev: {
            src: [
                '<%= project.dist %>/headerModule/*.html',
                '<%= project.dist %>/titleModule/*.html',
                '<%= project.dist %>/assetModule/*.html',
                '<%= project.dist %>/socialModule/*.html',
                '<%= project.dist %>/footerModule/*.html'
            ],
            dest : '<%= project.dev %>/index.html'
        }
    },
    prettify: {
        options: {
            indent : 4,
            brace_style : 'expand'
        }
    },
    clean: ['dist/**/*.html']
});

  //====================//
  // load grunt modules //
  //====================//
  grunt.loadNpmTasks('grunt-contrib-connect');
  // grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  // grunt.loadNpmTasks('grunt-sync');
  // grunt.loadNpmTasks('grunt-ftp-deploy');
  // grunt.loadNpmTasks('grunt-assemble');
  // grunt.loadNpmTasks('grunt-contrib-clean');
  // grunt.loadNpmTasks('grunt-contrib-concat');

  //=======================//
  // register task for CLI //
  //=======================//

  //== Active Development ==//
  grunt.registerTask('dev', [
    'connect:dev',
    'watch'
  ]);

  //== Move to Local QC and Checks to see if ready for FTP ==//
  grunt.registerTask('deploy', [
    'copy:stage',
    'connect:stage',
    'assemble'
  ]);
};
